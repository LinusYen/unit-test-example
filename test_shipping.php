<?php

use Services\Shipping;

require __DIR__ . '/vendor/autoload.php';

$amount = $argv[1];
$shipping = new Shipping;
try {
    $shippingFee = $shipping->calcFee($amount);
    echo "Amount: $" . $amount . ", ";
    echo "Shipping Fee: $" . $shippingFee . "\n";
} catch (\Exception $e) {
    echo $e->getMessage() . "\n";
}
