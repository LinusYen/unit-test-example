<?php

namespace Services;

class Cart
{
    public function checkout($amount)
    {
        $shippingFee = (new Shipping)->calcFee($amount);
        $totalAmount = $amount + $shippingFee;

        return $totalAmount;
    }
}