<?php
namespace Services;

class Shipping
{
    const DEFAULT_FEE = 80; // 預設運費

    public function calcFee($amount) {
        $fee = static::DEFAULT_FEE;

        if ($amount >= 399) {
            // 滿 399 免運
            $fee = 0;
        } elseif ($amount >= 199) {
            // 滿 199 運費 5 折
            $fee = $fee * 0.5;
        } elseif ($amount < 0) {
            throw new \Exception('金額不可小於 0');
        }

        return $fee;
    }
}


