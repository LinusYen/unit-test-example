<?php

use \PHPUnit\Framework\TestCase;

/**
 * Class CartTest
 * @runTestsInSeparateProcesses
 */
class CartTest extends TestCase
{
    private $cart;
    private $mockShipping;

    protected function setUp()
    {
        parent::setUp();
        $this->mockShipping = Mockery::namedMock(\Services\Shipping::class, MockShipping::class);
        $this->mockShipping->shouldReceive('calcFee')
            ->andReturn(80);

        $this->cart = new Services\Cart;
    }

    /**
     * @dataProvider checkoutProvider
     */
    public function testCheckout($amount, $expected)
    {
        $totalAmount = $this->cart->checkout($amount);

        $this->assertEquals($expected, $totalAmount);
    }

    public function checkoutProvider()
    {
        return array(
            array(400, 480),
        );
    }
}

class MockShipping
{
    public static function calcFee() {}
}
