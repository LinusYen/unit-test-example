<?php

use Services\Shipping;
use PHPUnit\Framework\TestCase;

class ShippingTest extends TestCase
{
    private $shipping;

    protected function setUp()
    {
        parent::setUp();
        $this->shipping = new Shipping;
    }

    /**
     * @dataProvider calcFeeProvider
     * @param $amount
     * @param $expected
     */
    public function testCalcFee($amount, $expected)
    {
        // $amount = 20;
        $fee = $this->shipping->calcFee($amount);

        // $this->assertEquals(80, $fee);
        $this->assertEquals($expected, $fee);
    }

    public function calcFeeProvider()
    {
        return array(
            array(100, 80),
            array(199, 40),
            array(399, 0),
        );
    }

    /**

     */
    public function testCalcFeeWithException()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('金額不可小於 0');

        $amount = -20;
        $fee = $this->shipping->calcFee($amount);

    }
}
