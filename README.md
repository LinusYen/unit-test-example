# Unit Test Example
## 下載＆安裝相依套件
```
git clone git@gitlab.com:LinusYen/unit-test-example.git
cd unit-test-example
composer install
```
---
## 如何執行 phpunit
在 `unit-test-example` 資料夾下
### 執行所有 test case
```
vendor/bin/phpunit
```
### 執行特定 test case (以`ShippingTest`為例)
```
vendor/bin/phpunit tests/Services/ShippingTest.php
```
### Coverage Report
路徑 `unit-test-example/report/`
